document.addEventListener("DOMContentLoaded",()=>{
    const currentMoveSelector=document.querySelector(".current__step__img")
    const gameCells=document.querySelectorAll(".cell");
    console.log(currentMoveSelector);
    const gameField=new GameField(currentMoveSelector,gameCells);
});

class GameField {

    currentMoveSelector;
    currentMoveValue;
    gameCells;
    gameField;

   
    constructor(moveSelector,cells) {
        this.currentMoveSelector=moveSelector; 
        this.currentMoveSelector.classList.add("current__step__img--cross");
        this.currentMoveValue="X";
        this.gameCells=cells;
        this.gameField=[
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];

        this.initCellsListeners(this.gameCells);
    }

    
    //Проверка строк
    checkRows(){
        for(let i=0;i<3;i++){
            if(this.gameField[i][1]==this.currentMoveValue){
                if(this.gameField[i][0]==this.gameField[i][1] && this.gameField[i][1]==this.gameField[i][2]){
                    return true;
                }
            }
        }
        return false;
    }
    //Инициализация ячеек для заполнения
    initCellsListeners(gameCells){
        for(let i=0;i<gameCells.length;i++){
            gameCells[i].addEventListener("click",()=>{
                const row=Math.trunc(i/3);
                const cell=i%3;
                if(this.checkCellAvailability(row,cell)){
                    this.gameField[row][cell]=this.currentMoveValue;
                    this.insertSymbol(gameCells[i]);
                    if( this.isOverGame()){
                        this.clearGameField();
                    }
                    else{
                        this.switchSymbol();
                    }
                    console.log(this.gameField);
                }
                else{
                    alert("Эта ячейка уже занята!")
                }
            });
         }
    }
    //Проверка того, что кто-то заполнил три в столбец
    checkCells(){
        for(let i=0;i<3;i++){
            if(this.gameField[1][i]==this.currentMoveValue){
                if(this.gameField[0][i]==this.gameField[1][i] && this.gameField[1][i]==this.gameField[2][i]){
                    return true;
                }
            }
        }
        return false;
    }

    //Проверка заполнения диагоналей одним символом
    checkDiagonal(){
        if(this.gameField[0][0]==this.gameField[1][1]
            && this.gameField[0][0]==this.gameField[2][2]
            && this.gameField[2][2]==this.currentMoveValue){
            return true;
        }
        else if(this.gameField[0][2]==this.gameField[1][1]
            &&this.gameField[1][1]==this.gameField[2][0]
            && this.gameField[1][1]==this.currentMoveValue){
            return true;
        }
        return false;
    }

    //Проверка того, что наступила ничья или кто-то победил
    isOverGame(){
        if(this.checkCells() || this.checkDiagonal() || this.checkRows()){
            alert(`Победил ${this.currentMoveValue}`);
            return true;
        }
        else if(this.checkDraw()){
            alert('Ничья!')
        }
        return false;
    }

    //Ввод символа
    insertSymbol(cell){
        if(this.currentMoveValue=="X"){
            cell.classList.add("cell__cross");
        }
        else{
            cell.classList.add("cell__zero");
        }
    }

    //Проверка, что ячейка свободна
    checkCellAvailability(row,cell){
        if(this.gameField[row][cell]==null){
            return true;
        }
        else return false;
    }

    //Смена хода
    switchSymbol(){
        if(this.currentMoveValue=="X"){
            this.currentMoveValue="O";
            this.currentMoveSelector.classList.add("current__step__img--cross")
            this.currentMoveSelector.classList.remove("current__step__img--zero")
        }
        else{
            this.currentMoveValue="X";
            this.currentMoveSelector.classList.add("current__step__img--zero");
            this.currentMoveSelector.classList.remove("current__step__img--cross")
        }
    }

    //Очистка игрового поля
    clearGameField(){
        this.gameField=[
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];
        console.log(this.gameField);

       
        for(let i=0;i<this.gameCells.length;i++){
            this.gameCells[i].classList.remove('cell__zero');
            this.gameCells[i].classList.remove('cell__cross');
        }

        this.currentMoveValue="X";
        this.currentMoveSelector.classList.remove("current__step__img--zero");
        this.currentMoveSelector.classList.add("current__step__img--cross");
    }

    //Проверка ничьи
    checkDraw(){
        for(let i=0;i<this.gameField.length;i++){
            for(let j=0;j<this.gameField[i].length;j++){
                if(this.gameField[i][j]==null){
                    console.log(1);
                    return false;
                }
            }
        }
        return true;
    }
  }


