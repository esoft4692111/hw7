document.addEventListener("DOMContentLoaded",()=>{


    const formFields=document.querySelectorAll(".auth__input");
    const submitBtn=document.querySelector(".submit");
    const regular=/^(?![\d+_@.-]+$)*[a-zA-Z0-9+_.-]*$/;


    const init=()=>{
        for(let i=0;i<formFields.length;i++){
            formFields[i].addEventListener("change",()=>{
                if(regular.test(formFields[i].value) && formFields[i].value.trim().length!=0){
                    formFields[i].classList.remove("password__incorrect"); 
                    if(checkAllInputs() && formFields[i].value.trim().length!=0){
                        submitBtn.classList.add("accept__btn");
                        submitBtn.disabled=false;
                    } 
                }
                else{
                    formFields[i].classList.add("password__incorrect");
                    submitBtn.classList.remove("accept__btn");
                    submitBtn.disabled=true;
                }
            })
    
            formFields[i].addEventListener("copy",(e)=>{
                e.preventDefault();
            })
    
            formFields[i].addEventListener("paste",(e)=>{
                e.preventDefault();
            })
        }
        submitBtn.addEventListener("click",()=>{
            if(submitBtn.classList.contains("accept__btn")){
                console.log(`Логин: ${formFields[0].value} Пароль:${formFields[1].value}`);
                formFields[0].value="";
                formFields[1].value="";
                submitBtn.classList.remove("accept__btn");
            }
        })
    
        submitBtn.addEventListener("click",(e)=>{
            e.preventDefault();
            e.stopImmediatePropagation();
        })
    }
   
    const checkAllInputs=()=>{
        for(let i=0;i<formFields.length;i++){
            if(formFields[i].classList.contains("password__incorrect") || formFields[i].value.trim().length==0){
                return false;
            }
        }
        return true;
    }

    init();
})